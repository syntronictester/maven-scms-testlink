import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import testlink.api.java.client.TestLinkAPIClient;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;


public class AdminLogin_Test {
	
	WebDriver driver;
	
	// Substitute your Dev Key Here
    public final String DEV_KEY = "ce14ea92a03d204e3433c9aef22ac99c";
 
    // Substitute your Server URL Here
    public final String SERVER_URL = "http://localhost:8082/testlink3/lib/api/xmlrpc/v1/xmlrpc.php";
 
   // Substitute your project name Here
    public final String PROJECT_NAME = "MavenWebDriver Project";
 
    // Substitute your test plan Here
    public final String PLAN_NAME = "MWD Test Plan";
 
    // Substitute your build name
    public final String BUILD_NAME = "MWD Build";
    
	@BeforeMethod
	public void setup() throws Exception
	{
		driver = new FirefoxDriver();

	    String baseUrl ="https://10.0.100.58:8181/admin/pages/login.xhtml";
	    driver.get(baseUrl);
	    
	}
	
	@Test
	public void adminTest() throws Exception {
		
		String result = "";
		String exception = null;
		
		try{
		  driver.findElement(By.name("j_username")).clear();
		  driver.findElement(By.name("j_username")).sendKeys("serenechoh");             //username input field identification and data entered
		  Thread.sleep(1000);                                               //this is just sleep command to wait for 1000 ms. 
		  result = TestLinkAPIResults.TEST_PASSED;
		  updateTestLinkResult("MWD-3", null, result);
		} catch (Exception ex){
			result = TestLinkAPIResults.TEST_FAILED;
			exception = ex.getMessage();
			updateTestLinkResult("MWD-3", exception, result);
		}
		
		try{
		  driver.findElement(By.name("j_password")).clear();
		  driver.findElement(By.name("j_password")).sendKeys("1234567C");         //Password input field identification and data entered
		  Thread.sleep(1000);  
		  result = TestLinkAPIResults.TEST_PASSED;
		  updateTestLinkResult("MWD-4", null, result);
		} catch (Exception ex){
			result = TestLinkAPIResults.TEST_FAILED;
			exception = ex.getMessage();
			updateTestLinkResult("MWD-4", exception, result);
		}
		
		  driver.findElement(By.className("btn")).click();                 //Login button identification and click it
		  Thread.sleep(1000);                                                 //this is just sleep command to wait for 1000 ms.
	  
		  driver.findElement(By.linkText("LOGOUT")).click();
	}

	@AfterMethod
	public void tearDown()
	{
		driver.close();
		  driver.quit(); 
	}
	
	public void updateTestLinkResult(String testCase, String exception, String result){
        TestLinkAPIClient testlinkAPIClient = new TestLinkAPIClient(DEV_KEY,
                               SERVER_URL);
        try {
			testlinkAPIClient.reportTestCaseResult(PROJECT_NAME, PLAN_NAME,
			                       testCase, BUILD_NAME, exception, result);
		} catch (TestLinkAPIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
   }

}
